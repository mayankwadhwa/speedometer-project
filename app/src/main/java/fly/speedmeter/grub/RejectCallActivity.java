package fly.speedmeter.grub;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class RejectCallActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reject_call);

        // Get intent object sent from the IncomingCallReceiver
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        TextView tv = (TextView) findViewById(R.id.txtmessage);
        if (b != null) {
            // Display rejected number in the TextView
            tv.setText(b.getString("number"));
        }

    }

}
