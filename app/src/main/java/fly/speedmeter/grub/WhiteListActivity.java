package fly.speedmeter.grub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class WhiteListActivity extends AppCompatActivity {

    private final int PICK_CONTACT1 = 1;
    private final int PICK_CONTACT2 = 2;
    private final int PICK_CONTACT3 = 3;
    private Uri uriContact;
    private String contactID;
    public static final int RequestPermissionCode = 1;
    public TextView txtView1;
    public TextView txtView2;
    public TextView txtView3;
    public TextView txtView4;
    public TextView txtView5;
    public TextView txtView6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_white_list);
        txtView1 = (TextView) findViewById(R.id.txtView1);
        txtView2 = (TextView) findViewById(R.id.txtView2);
        txtView3 = (TextView) findViewById(R.id.txtView3);
        txtView4 = (TextView) findViewById(R.id.txtView4);
        txtView5 = (TextView) findViewById(R.id.txtView5);
        txtView6 = (TextView) findViewById(R.id.txtView6);
        // EnableRuntimePermission();
    }

    public void callContacts1(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT1);
    }

    public void callContacts2(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT2);
    }

    public void callContacts3(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT3);
    }

    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        String contactNumber = null;
        if (reqCode == PICK_CONTACT1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                String number = "";
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        number = phones.getString(phones.getColumnIndex
                                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                        Toast.makeText(this, number, Toast.LENGTH_LONG).show();
                        txtView1.setText(number);

                        Data.getInstance().addWhiteListContact(number);

                    }

                    phones.close();
                    //Do something with number
                } else {
                    Toast.makeText(getApplicationContext(), "This contact has no phone number", Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }
        } else if (reqCode == PICK_CONTACT2) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                String number2 = "";
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        number2 = phones.getString(phones.getColumnIndex
                                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                        Toast.makeText(this, number2, Toast.LENGTH_LONG).show();
                        txtView2.setText(number2);

                        Data.getInstance().addWhiteListContact(number2);

                    }

                    phones.close();
                    //Do something with number
                } else {
                    Toast.makeText(getApplicationContext(), "This contact has no phone number", Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }
        } else if (reqCode == PICK_CONTACT3) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                String number3 = "";
                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
                cursor.moveToFirst();
                String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                if (hasPhone.equals("1")) {
                    Cursor phones = getContentResolver().query
                            (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                            + " = " + contactId, null, null);
                    while (phones.moveToNext()) {
                        number3 = phones.getString(phones.getColumnIndex
                                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                        Toast.makeText(this, number3, Toast.LENGTH_LONG).show();
                        txtView3.setText(number3);

                        Data.getInstance().addWhiteListContact(number3);

                    }

                    phones.close();
                    //Do something with number
                } else {
                    Toast.makeText(getApplicationContext(), "This contact has no phone number", Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }
        }
    }

    public void saveInfo(View v) {
        SharedPreferences sharedPref = getSharedPreferences("WhiteListInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Contact1", txtView1.getText().toString());
        editor.putString("Contact2", txtView2.getText().toString());
        editor.putString("Contact3", txtView3.getText().toString());
        editor.commit();
        Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();

    }

    public void displayData(View v) {
        SharedPreferences sharedPref = getSharedPreferences("WhiteListInfo", Context.MODE_PRIVATE);
        String c1 = sharedPref.getString("Contact1", " ");
        String c2 = sharedPref.getString("Contact2", " ");
        String c3 = sharedPref.getString("Contact3", " ");
        // Toast.makeText(this,c1, Toast.LENGTH_LONG).show();
        txtView4.setText(c1);
        txtView5.setText(c2);
        txtView6.setText(c3);


    }



   /* public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                    WhiteListActivity.this,
                Manifest.permission.READ_CONTACTS)) {

            Toast.makeText(WhiteListActivity.this, "CONTACTS permission allows us to Access CONTACTS app", Toast.LENGTH_LONG).show();
        } else {

            ActivityCompat.requestPermissions(WhiteListActivity.this, new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(WhiteListActivity.this, "Permission Granted, Now your application can access CONTACTS.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(WhiteListActivity.this, "Permission Canceled, Now your application cannot access CONTACTS.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    } */
}
